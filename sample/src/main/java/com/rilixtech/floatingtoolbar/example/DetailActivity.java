package com.rilixtech.floatingtoolbar.example;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.rilixtech.floatingtoolbar.FloatingToolbar;

public class DetailActivity extends AppCompatActivity implements FloatingToolbar.MorphListener {

  private FloatingActionButton mFabAppBar;
  private FloatingActionButton mFab;
  private AppBarLayout mAppBar;
  private FloatingToolbar mFloatingToolbar;
  private boolean mShowingFromAppBar;
  private boolean mShowingFromNormal;

  // 808100900

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.detail_activity);

    mAppBar = findViewById(R.id.appbar);
    mFloatingToolbar = findViewById(R.id.floatingToolbar);
    mFabAppBar = findViewById(R.id.fab);
    mFab = findViewById(R.id.fab2);

    // Don't handle attachFab click since we'll have 2 of them
    mFloatingToolbar.handleFabClick(false);
    mFloatingToolbar.addMorphListener(this);

    mFab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        mAppBar.setExpanded(false, true);
        mFloatingToolbar.attachFab(mFab);
        mFloatingToolbar.show();
        mShowingFromNormal = true;
      }
    });

    mFabAppBar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (!mFloatingToolbar.isShowing()) {
          mFab.hide();
          mFloatingToolbar.attachAppBarLayout(mAppBar);
          mFloatingToolbar.attachFab(mFabAppBar);
          mFloatingToolbar.show();
          mShowingFromAppBar = true;
        }
      }
    });
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    mFloatingToolbar.removeMorphListener(this);
  }

  @Override
  public void onMorphEnd() {

  }

  @Override
  public void onMorphStart() {

  }

  @Override
  public void onUnmorphEnd() {
    if (mShowingFromAppBar) {
      mFab.show();
    }

    if (mShowingFromNormal) {
      mAppBar.setExpanded(true, true);
    }

    mShowingFromAppBar = false;
    mShowingFromNormal = false;
  }

  @Override
  public void onUnmorphStart() {

  }
}
