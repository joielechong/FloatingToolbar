package com.rilixtech.floatingtoolbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Path;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;

class FloatingAnimatorLollipopImpl extends FloatingAnimator {

  private float mFabDiff;

  FloatingAnimatorLollipopImpl(FloatingToolbar toolbar) {
    super(toolbar);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  public void show() {
    super.show();

    final FloatingActionButton fab = getFab();
    final FloatingToolbar ftBar = getFloatingToolbar();
    Resources res = ftBar.getContext().getResources();

    ObjectAnimator anim = ObjectAnimator.ofFloat(fab, View.X, View.Y, createPath(true));
    anim.setInterpolator(new AccelerateDecelerateInterpolator());
    anim.setDuration(FAB_MORPH_DURATION + getDelay());
    anim.start();

    // Animate FAB elevation to 8dp
    anim = ObjectAnimator.ofFloat(fab, View.TRANSLATION_Z, res.getDimension(R.dimen.floatingtoolbar_translationz));
    anim.setInterpolator(new AccelerateDecelerateInterpolator());
    anim.setDuration(FAB_MORPH_DURATION + getDelay());
    anim.start();

    // Create circular reveal
    int width = ftBar.getWidth();
    int height = ftBar.getHeight();
    Animator toolbarReveal = ViewAnimationUtils.createCircularReveal(ftBar,
        width / 2, height / 2, (float) fab.getWidth() / 2f,
        (float) (Math.hypot(width / 2, height / 2)));

    toolbarReveal.setDuration(CIRCULAR_REVEAL_DURATION + getDelay());
    toolbarReveal.setTarget(this);
    toolbarReveal.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        super.onAnimationStart(animation);
        fab.setVisibility(View.INVISIBLE);
        ftBar.setVisibility(View.VISIBLE);
      }
    });

    toolbarReveal.setInterpolator(new AccelerateInterpolator());
    toolbarReveal.setStartDelay(CIRCULAR_REVEAL_DELAY + getDelay());
    toolbarReveal.start();

    // Animate FloatingToolbar elevation to 8dp
    anim = ObjectAnimator.ofFloat(ftBar, View.TRANSLATION_Z, res.getDimension(R.dimen.floatingtoolbar_translationz));
    anim.setDuration(CIRCULAR_REVEAL_DURATION + getDelay());
    anim.setStartDelay(CIRCULAR_REVEAL_DELAY + getDelay());
    anim.start();
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  public void hide() {
    super.hide();

    final FloatingActionButton fab = getFab();

    ObjectAnimator anim = ObjectAnimator.ofFloat(fab, View.X, View.Y, createPath(false));
    anim.setInterpolator(new AccelerateDecelerateInterpolator());
    anim.setDuration(FAB_UNMORPH_DURATION + getDelay());
    anim.setStartDelay(FAB_UNMORPH_DELAY + getDelay());
    anim.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        // Make sure the attachFab goes to the right place after the animation ends
        // when the Appbar is attached
        if (getAppBar() != null && fab.getY() != fab.getTop()) {
          fab.setAlpha(0f);
          fab.setY(fab.getTop());
          fab.animate().alpha(1f)
              .setDuration(200)
              .setInterpolator(new AccelerateDecelerateInterpolator()).start();
        }
        getAnimationListener().onAnimationFinished();
      }
    });
    anim.start();

    // Animate FAB elevation back to 6dp
    anim = ObjectAnimator.ofFloat(fab, View.TRANSLATION_Z, 0);
    anim.setInterpolator(new AccelerateDecelerateInterpolator());
    anim.setDuration(FAB_UNMORPH_DURATION + getDelay());
    anim.setStartDelay(FAB_UNMORPH_DELAY + getDelay());
    anim.start();

    final FloatingToolbar ftBar = getFloatingToolbar();

    int width = ftBar.getWidth();
    int height = ftBar.getHeight();

    Animator toolbarReveal = ViewAnimationUtils.createCircularReveal(ftBar,
        width / 2, height / 2, (float) (Math.hypot(width / 2, height / 2)),
        (float) getFab().getWidth() / 2f);

    toolbarReveal.setTarget(this);
    toolbarReveal.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        ftBar.setVisibility(View.INVISIBLE);
        fab.setVisibility(View.VISIBLE);
        // Ugly workaround for attachFab having wrong position for a few ms
        if (fab.getTranslationY() < 0) {
          fab.setAlpha(0f);
          fab.animate().alpha(1).setDuration(2);
        }
      }
    });

    toolbarReveal.setDuration(CIRCULAR_UNREVEAL_DURATION + getDelay());
    toolbarReveal.setInterpolator(new AccelerateInterpolator());
    toolbarReveal.setStartDelay(CIRCULAR_UNREVEAL_DELAY + getDelay());
    toolbarReveal.start();

    // Animate FloatingToolbar animation back to 6dp
    anim = ObjectAnimator.ofFloat(ftBar, View.TRANSLATION_Z, 0);
    anim.setDuration(CIRCULAR_UNREVEAL_DURATION + getDelay());
    anim.setStartDelay(CIRCULAR_UNREVEAL_DELAY + getDelay());
    anim.start();
  }

  private Path createPath(boolean show) {
    FloatingActionButton fab = getFab();
    FloatingToolbar ftBar = getFloatingToolbar();

    float fabOriginalX = fab.getLeft();
    float x2;
    float y2 = ftBar.getY();
    float endX;
    float endY;

    if (show) {
      mFabDiff = mFabDiff == 0 ? ftBar.getY() - fab.getY() : mFabDiff;
      endY = fab.getTop() + mFabDiff;
    } else {
      float transY = fab.getTranslationY();
      endY = transY < 0 ? fab.getTop() + transY - mFabDiff : fab.getTop();
    }

    if (!shouldMoveFabX()) {
      Path path = new Path();
      path.moveTo(fab.getX(), fab.getY());
      path.moveTo(fab.getX(), endY);
      return path;
    }

    if (show) {
      if (fabOriginalX > getRootView().getWidth() / 2f) {
        endX = fabOriginalX - getFab().getWidth();
      } else {
        endX = fabOriginalX + getFab().getWidth();
      }
    } else {
      endX = fabOriginalX;
    }

    Path path = new Path();
    path.moveTo(fab.getX(), fab.getY());

    if (fabOriginalX > getRootView().getWidth() / 2f) {
      x2 = fabOriginalX - fab.getWidth() / 4f;
    } else {
      x2 = fabOriginalX + fab.getWidth() / 4f;
    }

    path.quadTo(x2, y2, endX, endY);
    return path;
  }
}
