package com.rilixtech.floatingtoolbar;

import android.content.Context;

class Util {
  static float dpToPixels(Context context, int dp) {
    return dp * context.getResources().getDisplayMetrics().density;
  }
}
