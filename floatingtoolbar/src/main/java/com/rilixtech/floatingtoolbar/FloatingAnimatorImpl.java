package com.rilixtech.floatingtoolbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;

class FloatingAnimatorImpl extends FloatingAnimator {

  FloatingAnimatorImpl(FloatingToolbar toolbar) {
    super(toolbar);
  }

  @Override
  public void show() {
    super.show();

    final FloatingActionButton fab = getFab();
    final FloatingToolbar ftBar = getFloatingToolbar();

    int rootWidth = getRootView().getWidth();
    float endFabX;

    if (shouldMoveFabX()) {
      if (fab.getLeft() > rootWidth / 2f) {
        endFabX = fab.getLeft() - fab.getWidth();
      } else {
        endFabX = fab.getLeft() + fab.getWidth();
      }
    } else {
      endFabX = fab.getLeft();
    }

    PropertyValuesHolder xProp = PropertyValuesHolder.ofFloat(View.X, endFabX);
    PropertyValuesHolder yProp = PropertyValuesHolder.ofFloat(View.Y, ftBar.getY() * 0.95f);
    PropertyValuesHolder scaleXProp = PropertyValuesHolder.ofFloat(View.SCALE_X, 0);
    PropertyValuesHolder scaleYProp = PropertyValuesHolder.ofFloat(View.SCALE_Y, 0);

    ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(fab, xProp, yProp, scaleXProp, scaleYProp);
    animator.setDuration(FAB_MORPH_DURATION);
    animator.setInterpolator(new AccelerateInterpolator());
    animator.start();

    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(ftBar, "scaleX", 1f);
    objectAnimator.setDuration(CIRCULAR_REVEAL_DURATION);
    objectAnimator.setStartDelay(CIRCULAR_REVEAL_DELAY);
    objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
    objectAnimator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        ftBar.setVisibility(View.VISIBLE);
        fab.setVisibility(View.INVISIBLE);
      }
    });
    objectAnimator.start();
  }

  @Override
  public void hide() {
    super.hide();

    final FloatingActionButton fab = getFab();
    final FloatingToolbar ftBar = getFloatingToolbar();

    // A snackbar might have appeared, so we need to update the attachFab position again
    if (getAppBar() == null) {
      fab.setTranslationY(ftBar.getTranslationY());
    } else {
      fab.setY(ftBar.getY());
    }

    //final int fabNewY = getFab().getTop();
    ViewCompat.animate(fab)
        .x(fab.getLeft())
        .y(fab.getTop())
        .translationY(ftBar.getTranslationY())
        .scaleX(1f)
        .scaleY(1f)
        .setStartDelay(200)
        .setDuration(FAB_UNMORPH_DURATION)
        .setInterpolator(new AccelerateInterpolator())
        .setListener(new ViewPropertyAnimatorListenerAdapter() {
          @Override
          public void onAnimationStart(View view) {
            fab.setVisibility(View.VISIBLE);
          }

          @Override
          public void onAnimationEnd(View view) {
            // Make sure the attachFab goes to the right place after the animation ends
            // when the Appbar is attached
            if (getAppBar() != null && fab.getVisibility() == View.INVISIBLE) {
              fab.show();
            }
            getAnimationListener().onAnimationFinished();
          }
        });

    ViewCompat.animate(ftBar)
        .scaleX(0f)
        .setDuration(CIRCULAR_UNREVEAL_DURATION)
        .setStartDelay(CIRCULAR_UNREVEAL_DELAY)
        .setInterpolator(new AccelerateDecelerateInterpolator())
        .setListener(new ViewPropertyAnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(View view) {
            ftBar.setVisibility(View.INVISIBLE);
            ViewCompat.animate(ftBar).setListener(null);
          }
        });
  }
}
