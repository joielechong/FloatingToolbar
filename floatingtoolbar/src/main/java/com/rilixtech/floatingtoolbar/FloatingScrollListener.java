package com.rilixtech.floatingtoolbar;

import android.support.v7.widget.RecyclerView;

class FloatingScrollListener extends RecyclerView.OnScrollListener {

  private FloatingToolbar mToolbar;

  FloatingScrollListener(FloatingToolbar toolbar) {
    mToolbar = toolbar;
  }

  @Override
  public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
    if (newState == RecyclerView.SCROLL_STATE_DRAGGING && mToolbar.isShowing()) {
      mToolbar.hide();
    }
  }
}
